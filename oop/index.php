<?php

require_once('animal.php');
require_once("frog.php");
require_once("ape.php");

$sheep = new Animal("shaun");
$kodok = new Frog("buduk");
$sungokong = new Ape("kera sakti");

echo "Name : ".$sheep->name."<br>"; // "shaun"
echo "Legs : ".$sheep->legs."<br>"; // 4
echo "Cold Blooded : ".$sheep->cold_blooded."<br>"; // "no"

echo "<br>";

echo "Name : ".$kodok->name."<br>"; // "buduk"
echo "Legs : ".$kodok->legs."<br>"; // 4
echo "Cold Blooded : ".$kodok->cold_blooded."<br>"; // "no"
echo "Jump : ";
echo $kodok->jump()."<br>";

echo "<br>";

echo "Name : ".$sungokong->name."<br>"; // "shaun"
echo "Legs : ".$sungokong->legs."<br>"; // 4
echo "Cold Blooded : ".$sungokong->cold_blooded."<br>"; // "no"
echo "Yell : ";
echo $sungokong->yell()."<br>";




// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())



?>